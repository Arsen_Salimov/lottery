package com.example.arssa.lotery

import android.os.Bundle
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.view.KeyEvent
import com.example.arssa.lotery.Router.OnFragmentChangeListener
import com.example.arssa.lotery.Router.Router
import com.example.arssa.lotery.Router.RouterScreen
import com.example.arssa.lotery.Screens.Menu.MenuItem
import com.mikepenz.materialdrawer.AccountHeaderBuilder
import com.mikepenz.materialdrawer.Drawer
import com.mikepenz.materialdrawer.DrawerBuilder
import com.mikepenz.materialdrawer.model.DividerDrawerItem
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem
import com.mikepenz.materialdrawer.model.ProfileDrawerItem
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem
import com.nostra13.universalimageloader.core.ImageLoader
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity(),OnFragmentChangeListener {
    var drawer : Drawer? = null;
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var config : ImageLoaderConfiguration = ImageLoaderConfiguration.Builder(this)
                                                    .build();
        ImageLoader.getInstance().init(config);

        SetupNavigation();
        Router.Navigate(RouterScreen.LANDING)
    }

    private fun SetupNavigation() {
        setSupportActionBar(toolbar);
        supportActionBar?.setDisplayHomeAsUpEnabled(true);

        var headerResult = AccountHeaderBuilder()
                .withActivity(this)
                .withHeaderBackground(R.mipmap.header)

                .addProfiles(
                        ProfileDrawerItem().withName("Dimka Sark").withEmail("dimkasark@gmail.com").withIcon(R.mipmap.ic_launcher))
                .build();

        var builder = DrawerBuilder()
                .withActivity(this)
                .withToolbar(toolbar)
                .withAccountHeader(headerResult)
                //.withHeader(R.layout.menu_header);

        for(menuItem in MenuItem.values()){
            if(menuItem == MenuItem.MESSAGES){
                builder.addDrawerItems(PrimaryDrawerItem().withName(menuItem.Title).withTag(menuItem.name).withBadge("+150"));
            }else {
                builder.addDrawerItems(PrimaryDrawerItem().withName(menuItem.Title).withTag(menuItem.name));
            }
        }
        builder.withOnDrawerItemClickListener { view, i, iDrawerItem ->
                    var name = iDrawerItem.tag as String;
                    var findItem = MenuItem.valueOf(name);
                    if(findItem.Screen!=null){
                        Router.Navigate(findItem.Screen as RouterScreen)
                    } else{
                        Router.Navigate(RouterScreen.LANDING)
                    }
                    false
                };
        drawer = builder.build();

        Router.Init(this);
    }

    override fun OnFragmentChange(newFragment: RouterScreen?) {
        if(newFragment?.showMenu as Boolean){
            ShowNavigationMenu()
        } else {
            HideNavigationMenu()
        }
        toolbar.title = newFragment?.toolbarName;
    }

    fun ShowNavigationMenu(){
        if(!(drawer?.actionBarDrawerToggle?.isDrawerIndicatorEnabled as Boolean)){
            drawer?.drawerLayout?.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
            drawer?.actionBarDrawerToggle?.isDrawerIndicatorEnabled = true;
            supportActionBar?.setDisplayHomeAsUpEnabled(false);
        }
    }

    fun HideNavigationMenu(){
        if(drawer?.actionBarDrawerToggle?.isDrawerIndicatorEnabled as Boolean){
            drawer?.drawerLayout?.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            drawer?.actionBarDrawerToggle?.isDrawerIndicatorEnabled = false;
            supportActionBar?.setDisplayHomeAsUpEnabled(false);
        }
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if(keyCode == KeyEvent.KEYCODE_BACK && onActivityBackPressed()){
            return true;
        }
        return super.onKeyUp(keyCode, event)
    }

    fun onActivityBackPressed() : Boolean{
        if(drawer?.isDrawerOpen as Boolean){
            drawer?.closeDrawer();
            return true;
        }
        return false;
    }
}