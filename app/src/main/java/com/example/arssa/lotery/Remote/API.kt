package com.example.arssa.lotery.Remote

import com.example.arssa.lotery.Models.Lottery
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import java.util.*

/**
 * Created by arssa on 15-Feb-16.
 */
object API {
    private var client = OkHttpClient()
    private var retrofit = Retrofit.Builder()
        .baseUrl("https://google.com")
        .client(client)
        .build();

    private var RemoteLotteryService = retrofit.create(RemoteLotteryService::class.java)

    fun getLotteryList() : List<Lottery> {
        var lotteryList = ArrayList<Lottery>()
        lotteryList.add(Lottery("https://test.ltvod.tv/imgs/games/powerball/button-logo.jpg"))
        lotteryList.add(Lottery("https://test.ltvod.tv/imgs/games/megamillions/button-logo.jpg"))
        lotteryList.add(Lottery("https://test.ltvod.tv/imgs/games/hotlotto/logo.png"))

        return lotteryList;
    }
}
