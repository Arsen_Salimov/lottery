package com.example.arssa.lotery.Screens.LotteryList

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.arssa.lotery.R
import com.example.arssa.lotery.Remote.API
import com.example.arssa.lotery.Router.Router
import com.example.arssa.lotery.Router.RouterScreen
import kotlinx.android.synthetic.main.fragment_lottery_list.*

/**
 * Created by arssa on 15-Feb-16.
 */
class LotteryListFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_lottery_list,container, false);
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        lotteryList.layoutManager = GridLayoutManager(context,2)
        lotteryList.adapter = LotteryListAdapter(API.getLotteryList()) {
            Router.Navigate(RouterScreen.GAME,true)
        }
    }
}