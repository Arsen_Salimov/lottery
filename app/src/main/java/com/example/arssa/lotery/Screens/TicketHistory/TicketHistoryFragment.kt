package com.example.arssa.lotery.Screens.TicketHistory

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.arssa.lotery.R

/**
 * Created by dimka on 2/16/2016.
 */
class TicketHistoryFragment : Fragment(){
    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_ticket_history,container,false);
    }
}