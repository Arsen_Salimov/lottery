package com.example.arssa.lotery.Remote

import com.example.arssa.lotery.Models.Lottery
import retrofit2.Call
import retrofit2.http.GET

/**
 * Created by arssa on 15-Feb-16.
 */
interface RemoteLotteryService {
    @GET("")
    fun listLottery() : Call<List<Lottery>>
}
