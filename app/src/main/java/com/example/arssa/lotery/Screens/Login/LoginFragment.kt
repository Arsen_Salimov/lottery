package com.example.arssa.lotery.Screens.Login

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.arssa.lotery.R
import com.example.arssa.lotery.Router.Router
import com.example.arssa.lotery.Router.RouterScreen
import kotlinx.android.synthetic.main.fragment_login.*

/**
 * Created by arssa on 15-Feb-16.
 */
class LoginFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_login,container,false);
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btnLogin.setOnClickListener {
            Login(etLogin.text.toString(),etPassword.text.toString())
        }
    }

    private fun Login(login: String,password : String) {
        Router.Navigate(RouterScreen.GAME_LOBBY)
    }
}

