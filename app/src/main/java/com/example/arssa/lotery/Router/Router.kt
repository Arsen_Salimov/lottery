package com.example.arssa.lotery.Router

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v7.app.AppCompatActivity
import com.example.arssa.lotery.Screens.LotteryList.LotteryListFragment
import com.example.arssa.lotery.R
import com.example.arssa.lotery.Screens.Landing.LandingFragment
import com.example.arssa.lotery.Screens.Login.LoginFragment
import com.example.arssa.lotery.Screens.Lottery.LotteryFragment

/**
 * Created by arssa on 15-Feb-16.
 */
object Router {
    private var fragmentManager : FragmentManager? = null;
    private var onFragmentChangeListener : OnFragmentChangeListener? = null;

    fun <T> Init(activity : T)
            where T :AppCompatActivity,
                    T:OnFragmentChangeListener{
        fragmentManager = activity.supportFragmentManager;
        onFragmentChangeListener = activity;

    }

    fun Navigate(route : RouterScreen, addToBackStack : Boolean = false) {
        if (fragmentManager == null)
            throw IllegalStateException("Call Router.Init before")

        var transaction = fragmentManager
                ?.beginTransaction()
                ?.replace(R.id.fragmentContainer,route.CreateFragment());

        if (addToBackStack)
            transaction?.addToBackStack(null);

        onFragmentChangeListener?.OnFragmentChange(route);
        transaction?.commit()
    }
}
