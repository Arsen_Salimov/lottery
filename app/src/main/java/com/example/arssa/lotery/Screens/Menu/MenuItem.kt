package com.example.arssa.lotery.Screens.Menu

import com.example.arssa.lotery.Router.RouterScreen

/**
 * Created by dimka on 2/16/2016.
 */
enum class MenuItem {
    GAME_LOBBY(RouterScreen.GAME_LOBBY),
    TICKET_HISTORY(RouterScreen.TICKET_HISTORY),
    MESSAGES(RouterScreen.MESSAGES),
    MY_ACCOUNT(RouterScreen.MY_ACCOUNT),
    SIGN_OUT(title = "Sign Out");

    var Screen : RouterScreen? = null;
    var Title : String? = null;

    constructor(routerScreen: RouterScreen? = null, title:String? = null) {
        if (title == null && routerScreen!=null){
            Title = routerScreen.toolbarName;
        }else if(title!=null){
            Title = title;
        }
        Screen = routerScreen
    }
}