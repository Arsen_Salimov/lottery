package com.example.arssa.lotery.Screens.Landing

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.arssa.lotery.R
import com.example.arssa.lotery.Router.Router
import com.example.arssa.lotery.Router.RouterScreen
import kotlinx.android.synthetic.main.fragment_landing.*

/**
 * Created by arssa on 15-Feb-16.
 */
class LandingFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_landing,container,false);
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btnSignIn.setOnClickListener {
            Router.Navigate(RouterScreen.LOGIN);
        }
    }


}
