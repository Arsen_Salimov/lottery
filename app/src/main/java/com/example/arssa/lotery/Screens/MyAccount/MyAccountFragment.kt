package com.example.arssa.lotery.Screens.MyAccount

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.arssa.lotery.R
import com.example.arssa.lotery.Router.Router
import com.example.arssa.lotery.Router.RouterScreen
import kotlinx.android.synthetic.main.fragment_my_account.*

/**
 * Created by dimka on 2/16/2016.
 */
class MyAccountFragment : Fragment(){
    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_my_account,container,false);
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        signOut.setOnClickListener {
            Router.Navigate(RouterScreen.LANDING)
        }
    }
}