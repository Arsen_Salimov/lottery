package com.example.arssa.lotery.Screens.Lottery

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import android.util.AttributeSet
import android.util.Log
import android.view.MotionEvent
import android.view.View
import java.util.*

/**
 * Created by arssa on 15-Feb-16.
 */
class PickNumberView : View {
    private var ItemCount = 69;
    private var mTextWidth = 50f;
    private var mItemWidth = 200f;
    private var mStartX = 0f;
    private var mStartY = 25f;
    private var mRectRoundRadius = 30f;
    private var mOvalSize = mItemWidth - 50f;

    private var mTextPaint : Paint = Paint(Paint.ANTI_ALIAS_FLAG);
    private var mRectPaint: Paint = Paint(Paint.ANTI_ALIAS_FLAG);
    private var mRectSelectedPaint: Paint = Paint(Paint.ANTI_ALIAS_FLAG);

    private var mLastTouchPos = null

    private var mSelectedItems = ArrayList<Int>();
    private var mMaxItemToSelect = 1;

    var MaxItemToSelect : Int
    get() = mMaxItemToSelect
    set(value) {
        if (value < 0)
            throw IllegalArgumentException("Value can't be less then zero")

        mMaxItemToSelect = value
    }

    constructor(context: Context) : super(context){
        Init();
    }

    constructor(context: Context,attrs: AttributeSet) : super(context,attrs) {
        Init();
    }

    fun ClearSelection() {
        mSelectedItems.clear()
        invalidate()
    }

    private fun Init() {
        mTextPaint.color = Color.BLACK
        mTextPaint.textSize = mTextWidth;

        mRectPaint.style = Paint.Style.STROKE;
        mRectPaint.color = Color.BLACK;

        mRectSelectedPaint.color = Color.GREEN;
        mRectSelectedPaint.style = Paint.Style.FILL;
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        var minw = paddingLeft + paddingRight + suggestedMinimumWidth - 25;
        var w = resolveSizeAndState(minw, widthMeasureSpec, 1);

        var itemsInRow = (w / mItemWidth).toInt();
        var rows = ItemCount / itemsInRow;

        var h = ((1 + rows) * mItemWidth).toInt();
        setMeasuredDimension(w, h);
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        mStartX = width - mItemWidth * (width / mItemWidth).toInt();
        mStartX = 50f;

        var x = mStartX;
        var y = mStartY

        for (i in 1..ItemCount) {
            var str = i.toString()

            if (x + mItemWidth >= width) {
                x = mStartX;
                y += mItemWidth
            }

            var textWidth = mTextPaint.measureText(str)

            var ovalX = x + mItemWidth / 2;
            var ovalY = y + mItemWidth / 2;

            var textX = ovalX - textWidth / 2;
            var textY = ovalY - mTextPaint.ascent() / 2;

            var rect = RectF(ovalX - mOvalSize / 2,ovalY - mOvalSize / 2,ovalX + mOvalSize / 2, ovalY + mOvalSize / 2);
            if (i - 1 in mSelectedItems ) {
                canvas.drawRoundRect(rect,mRectRoundRadius,mRectRoundRadius, mRectSelectedPaint)
            }

            canvas.drawRoundRect(rect,mRectRoundRadius,mRectRoundRadius, mRectPaint)
            canvas.drawText(str,textX,textY, mTextPaint)

            x += mItemWidth
        }
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        var x = event.x - mStartX;
        var y = event.y - mStartY;
        var action = event.action;

        when (action) {
            MotionEvent.ACTION_DOWN -> {
                return true
            }
            MotionEvent.ACTION_UP -> {
                var row = (y / mItemWidth).toInt();
                var column = (x / mItemWidth).toInt();

                if (column < (width / mItemWidth).toInt()) {
                    var pos = column + row *  (width / mItemWidth).toInt()

                    if (pos !in mSelectedItems) {

                        if (mSelectedItems.size == MaxItemToSelect && MaxItemToSelect > 0)
                            mSelectedItems.removeAt(0);

                        mSelectedItems.add(pos);
                        invalidate()
                    }
                }
            }
        }

        return false;
    }
}