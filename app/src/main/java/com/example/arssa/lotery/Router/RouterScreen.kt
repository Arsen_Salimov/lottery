package com.example.arssa.lotery.Router

import android.support.v4.app.Fragment
import com.example.arssa.lotery.Screens.Landing.LandingFragment
import com.example.arssa.lotery.Screens.Login.LoginFragment
import com.example.arssa.lotery.Screens.Lottery.LotteryFragment
import com.example.arssa.lotery.Screens.LotteryList.LotteryListFragment
import com.example.arssa.lotery.Screens.MyAccount.MyAccountFragment
import com.example.arssa.lotery.Screens.TicketHistory.MessagesFragment
import com.example.arssa.lotery.Screens.TicketHistory.TicketHistoryFragment

/**
 * Created by dimka on 2/16/2016.
 */
enum class RouterScreen(val toolbarName : String = "",val showMenu : Boolean = true) {
    LANDING(showMenu = false){
        override fun CreateFragment(): Fragment {
            return LandingFragment();
        }
    },
    LOGIN("Login",showMenu = false){
        override fun CreateFragment(): Fragment {
            return LoginFragment();
        }
    },
    GAME_LOBBY("Game Lobby"){
        override fun CreateFragment(): Fragment {
            return LotteryListFragment();
        }
    },
    GAME(""){
        override fun CreateFragment(): Fragment {
            return LotteryFragment();
        }
    },
    MY_ACCOUNT("My Account"){
        override fun CreateFragment(): Fragment {
            return MyAccountFragment();
        }
    },
    TICKET_HISTORY("Ticket History"){
        override fun CreateFragment(): Fragment {
            return TicketHistoryFragment();
        }
    },
    MESSAGES("Messages"){
        override fun CreateFragment(): Fragment {
            return MessagesFragment();
        }
    };

    abstract fun CreateFragment(): Fragment
}