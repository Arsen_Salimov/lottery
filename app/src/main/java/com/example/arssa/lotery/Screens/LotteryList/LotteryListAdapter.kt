package com.example.arssa.lotery.Screens.LotteryList

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.arssa.lotery.Models.Lottery
import com.example.arssa.lotery.R
import com.nostra13.universalimageloader.core.ImageLoader
import kotlinx.android.synthetic.main.item_lottery_list.view.*

/**
 * Created by arssa on 15-Feb-16.
 */
class LotteryListAdapter(private var lotteryList: List<Lottery>,
                         private var itemClickListener: (Lottery) -> Unit)
    : RecyclerView.Adapter<LotteryListAdapter.ViewHolder>() {

    var imageLoader = ImageLoader.getInstance();

    override fun onBindViewHolder(holder: ViewHolder, pos: Int) {
        var lottery = lotteryList[pos];
        holder.itemView.setOnClickListener {
            itemClickListener(lottery);
        }

        imageLoader.displayImage(lottery.image, holder.itemView?.ivLotteryLogo);
    }

    override fun getItemCount(): Int = lotteryList.count()

    override fun onCreateViewHolder(parent: ViewGroup?, pos: Int): ViewHolder? {
        var rootView = LayoutInflater.from(parent?.context)
                                    .inflate(R.layout.item_lottery_list,parent,false);

        return ViewHolder(rootView);
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    }
}
