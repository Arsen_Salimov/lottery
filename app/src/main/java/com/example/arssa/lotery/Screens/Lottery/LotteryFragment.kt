package com.example.arssa.lotery.Screens.Lottery

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.arssa.lotery.R
import kotlinx.android.synthetic.main.fragment_lottery.*

/**
 * Created by arssa on 15-Feb-16.
 */
class LotteryFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_lottery,container,false);
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        pickUpNumberView.MaxItemToSelect = 5;
        btnClear.setOnClickListener {
            pickUpNumberView.ClearSelection()
        }
        btnFavorites.setOnClickListener {

        }
        btnNext.setOnClickListener {

        }
    }
}